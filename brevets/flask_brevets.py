"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    # If it doesn't find km, it defaults to 999
    km = request.args.get('km', 999, type=float)
    # Converts it to a string, strips off km, then converts it to an int
    # distance = int(str(request.args.get('distance', 999))[:-2])
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # Gets the date & time & distance
    date = request.args.get("date")
    time = request.args.get("time")
    distance = request.args.get("distance")
    datetime = date + " " + time
    # Format it into arrow
    r = arrow.get(datetime, 'YYYY-MM-DD HH:mm')
    # Use arrow.now as a base then replace the fields
    newtime = arrow.now()
    newtime = newtime.replace(year=r.year, month=r.month, day=r.day, hour=r.hour, minute=r.minute)

    open_time = acp_times.open_time(km, distance, newtime)
    close_time = acp_times.close_time(km, distance, newtime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
