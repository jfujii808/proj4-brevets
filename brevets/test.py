"""
Nose tests
"""
import arrow
import acp_times 


r = arrow.get('2017-01-01 01:00', 'YYYY-MM-DD HH:mm')
base = arrow.now()
starttime = base.replace(year=r.year, month=r.month, day=r.day, hour=r.hour, minute=r.minute)

def test_start():
    """
    Tests if the start opens at the beginning
    Should be 01/01 1:00
    """
    assert acp_times.open_time(0, 200, starttime) == starttime.isoformat()

def test_close():
    """
    Tests if the start closes 1 hour after it opens
    Should be 01/01 2:00
    """
    closestart = starttime.shift(hours=+1)
    assert acp_times.close_time(0, 200, starttime) == closestart.isoformat()

def test_200km():
    """
    Tests if the 200km works right when km is 50
    Should be 01/01 2:28
    """
    checkpoint = starttime.shift(hours=+1, minutes=+28)
    print(acp_times.open_time(50, 200, starttime))
    print(checkpoint.isoformat())
    assert acp_times.open_time(50, 200, starttime) == checkpoint.isoformat()

def test_205km():
    """
    Tests if the 200km works right when km is over 200
    Should be 01/01 6:53
    """
    pastend = starttime.replace(hours=5, minutes=53)
    print(acp_times.open_time(205, 200, starttime))
    print(pastend.isoformat())
    assert acp_times.open_time(205, 200, starttime) == pastend.isoformat()

def test_300km_brevet():
    """
    Tests if it works with a brevet distance that isn't 200
    Should be 01/01 2:28
    """
    checkpoint = starttime.shift(hours=+1, minutes=+28)
    print(acp_times.open_time(50, 300, starttime))
    print(checkpoint.isoformat())
    assert acp_times.open_time(50, 300, starttime) == checkpoint.isoformat()


    
