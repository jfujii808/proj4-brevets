"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
def calculate_time(control_dist_km, brevet_dist_km, brevet_start_time, OPEN):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
       SPEED: A list of thresholds for brevet racing containing tuples
           of (KM, min speed, max speed)
       OPEN: A Boolean value determining which mode to use
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # When overlapping thresholds, prioritize the smaller one
    # SPEED entries are indexed as (KM threshold stop, min speed, max speed)
    SPEED = [(200,15,34),(400,15,32),(600,15,30),(1000,11.428,28),(1300,13.333,26)]

    # If the checkpoint km distance is over, set it to the brevet distance
    # Otherwise, keep it as is
    if control_dist_km > int(brevet_dist_km):
        checkpoint = int(brevet_dist_km)
    else:
        checkpoint = int(control_dist_km)
    newtime = brevet_start_time

    if control_dist_km == 0:
        # Makes the open at the begin_time/date
        # Makes the close exactly 1 hour after it opens
        newclose = newtime.shift(hours=+1)
        open_time = newtime.isoformat()
        close_time = newclose.isoformat()
        if OPEN == True:
            return open_time
        else:
            return close_time
        
    for threshold in SPEED:
        marker, minspd, maxspd = threshold
        # If the checkpoint mark is in the threshold's stopping point
        if checkpoint <= marker:
            # If we're going for opening time
            if OPEN == True:
                time = checkpoint/maxspd
                hours = int(time) # This will give a rounded down integer
                mins = int(round((time-hours)*60)) # Gives an integer for minutes
                # Shifts the given time
                newtime = newtime.shift(hours=+hours, minutes=+mins)
            else:
                time = checkpoint/minspd
                hours = int(time) # This will give a rounded down integer
                mins = int(round((time-hours)*60)) # Gives an integer for minutes
                # Shifts the given time
                newtime = newtime.shift(hours=+hours, minutes=+mins)
            return newtime.isoformat()
            
        
    
def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
      
    return calculate_time(control_dist_km, brevet_dist_km, brevet_start_time, True)


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    return calculate_time(control_dist_km, brevet_dist_km, brevet_start_time, False)
