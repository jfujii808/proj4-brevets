# Project 4: Brevet time calculator with Ajax
Description: This implements the RUSA ACP controle time calculator with flask and AJAX.
User Instructions: 
1) Put in the date, time, and how long the brevet will be in km via the dropdown menus
2) Put in a start time first at 0km. It should start at the provided date and time and close exactly 1 hour after
3) Input any checkpoints that are between 0km and whatever brevet distance you chose
4) Input a closing checkpoint at your brevet distance or slightly more (If more it will not give you more time)
Developer Instructions:
acp_times has a helper function that open_times and close_times depend on.
All changes should be documented via comments
Any contextual information is provided by existing comments
Author: Jonathan Fujii
Contact Address: jfujii@oregon.edu


